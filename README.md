# Xbunto
Installation de xubuntu en DualBoot

Prérequis
Pour l'installation de xubuntu en dualboot, il vous faudra les prérequis suivants:

Un ordinateur où installer xubuntu
Une clé bootable avec xubuntu installé dessus

Démarrage sur la clé
Notre clé USB étant désormais configuré, nous pouvons redémarrer l'ordinateur sur la clé USB avec l'aide du menu de démarrage du BIOS de notre ordinateur (Dans notre cas, nous avons dû accéder à l'interface du BIOS pour pouvoir accéder au menu de démarrage).
Une fois notre ordinateur démarré, il nous affiche un menu où l'on peut choisir l'OS dont nous aurons besoin. Nous choissons alors à l'aide des flèches du clavier notre fichier Xubuntu et appuyer sur entrer.

Configuration préliminaire et installation de Xubuntu


Xubuntu démarre et commence un contrôle des fichiers, que nous pouvons éventuellement arrêter avec le raccourci ctrl+c.


Après quelques secondes, xubuntu nous demande de choisir une langue. Si vous choisissez une autre langue que le français, veillez à ce que la disposition du clavier soit en français (azerty) puis poursuivre l'installation.


Différentes options nous sont proposés, mais elles ne sont pas essentielle pour l'installation. Choisissez comme vous le souhaitez. Pour notre part, nous choisirons l'installation des logiciels tiers pour notre matériel graphique.


Le menu suivant nous demande comment nous souhaitons installer Xubuntu sur notre disque. Pour installer en dualboot, nous choisissons le mode avancé "Autre chose".


Pour installer Xubuntu sur notre disque, il faut repartitionner notre disque, en diminuant la taille de notre partition actuelle, de façon à laisser place à une nouvelle partition primaire, en système de fichiers journalisé ext4 (32go devrait faire l'affaire).


Ne pas oublier de préciser le point de montage sur la partition. (Par défaut, nous choisirons "/" comme point de montage, puis cliquer sur "installer maintenant".


Choisir le lieu d'installation puis cliquer sur "continuer".
Entrer les différentes informations demandé, puis cliquer de nouveau sur "continuer" pour commencer l'installation sur votre disque.


Une fois l'installation terminé, enlever la clé de démarrage, puis redémarrer l'ordinateur.
